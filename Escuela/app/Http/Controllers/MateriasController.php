<?php

namespace App\Http\Controllers;

use App\Models\Materia;
use App\Models\usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class MateriasController extends Controller
{
    public function crearMateria(){
        $usuario=usuario::where('correo',Session::get('usuario')->correo)->first();
        if($usuario->role_id != 1){
            if($usuario->role_id == 2){
                return view("perfilprofesor");
            }
            if($usuario->role_id == 3){
                return view("perfil");
            }
        }
        else{
            return view("altaMateria");
        }
    }
    public function altaMateria(request $datos){
        $nombreMateria=$datos->nombreMateria;
        $profesor=$datos->profesor;
        $id=$datos->idProfesor;
        $carreraAsignacion=$datos->carrera;
        $grupo=$datos->grupo;
        $usuario=usuario::where('id',$id)->where('role_id',2)->first();
        if (!$usuario){
            return view("altaMateria",["estatus"=> "error", "mensaje"=> "¡Verifique los datos del profesor!"]);
        }
        $materia=new Materia();
        $materia->nombre_materia=$nombreMateria;
        $materia->nombre_maestro=$profesor;
        $materia->id_maestro=$id;
        $materia->carrera_asignada=$carreraAsignacion;
        $materia->grupo_asignado=$grupo;
        $materia->save();
        return view("altaMateria",["estatus"=> "success", "mensaje"=> "¡Materia Registrado Exitosamente!"]);
    }

    public function mostrarMaterias(){
        $materiasR=Materia::all();
        return view('materiasRegistradas',["materiasR" => $materiasR]);
    }
    public function eliminarMateria(request $datos){
        $id=$datos->idMateria;
        $materia=Materia::find($id);
        if($materia){
            $materia->delete();
            return view("PerfilAdmin",["estatus"=> "success", "mensaje"=> "Materia Eliminada Correctamente"]);
        }
    }
    public function buscarMaterias(){
        $mismaterias=Materia::where('id_maestro',Session::get('usuario')->id)->get();
        return view('materiasAsignadas',["mismaterias" => $mismaterias]);
    }
    public function mostrarAlumnoMateria(request $datos){
        $materia=$datos->materia;
        $grup=$datos->grupo;

        $grupo=Materia::where('nombre_materia',$materia)->where('grupo_asignado',$grup)->first();
        if ($grupo){
            $alum=usuario::all()->where('grupo',$grup);
            return view('mostrargrupos',["alum" => $alum,$materia]);
        }else{
            echo 'error';
        }
    }
}
