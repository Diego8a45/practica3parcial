<?php

namespace App\Http\Controllers;

use App\Models\Materia;
use App\Models\usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Testing\Fluent\Concerns\Has;
use function Symfony\Component\VarDumper\Dumper\esc;

class AlumnoController extends Controller
{

    public function bienvenido(){
        return view("Bienvenido");
    }
    public function nosotros(){
        return view("nosotros");
    }
    public function carreras(){
        return view("carreras");
    }
    public function registro(){
        return view("registro");
    }
    public function inicioSesion(){
        return view("inicioSesion");
    }
    public function alta(){
        $usuario=usuario::where('correo',Session::get('usuario')->correo)->first();
        if($usuario->role_id != 1){
            if($usuario->role_id == 2){
                return view("perfilprofesor");
            }
            if($usuario->role_id == 3){
                return view("perfil");
            }
        }
        else{
            return view("altaAlumno");
        }
    }
    public function altaAdmin(){
        $usuario=usuario::where('correo',Session::get('usuario')->correo)->first();
        if($usuario->role_id != 1){
            if($usuario->role_id == 2){
                return view("perfilprofesor");
            }
            if($usuario->role_id == 3){
                return view("perfil");
            }
        }
        else{
            return view("altaAdmin");
        }
    }
    public function altaProfesor(){
        $usuario=usuario::where('correo',Session::get('usuario')->correo)->first();
        if($usuario->role_id != 1){
            if($usuario->role_id == 2){
                return view("perfilprofesor");
            }
            if($usuario->role_id == 3){
                return view("perfil");
            }
        }
        else{
            return view("altaProfesor");
        }
    }
    public function perfil(){
        $usuario=usuario::where('correo',Session::get('usuario')->correo)->first();
        if($usuario->role_id == 3){
            return view("perfil");
        }else if($usuario->role_id == 2){
            return view("perfilprofesor");
        }else if($usuario->role_id == 1 ){
            return view("perfilAdmin");
        }
    }
    public function editarDatos(){
        return view("editar");
    }

    public function registrar(request $datos)
    {
        $usuario = usuario::where('correo',$datos->email)->first();
        if($usuario){
            return view("registro",["estatus"=> "error", "mensaje"=> "¡El correo ya se encuentra registrado!"]);
        }

        $nombres = $datos->nombre;
        $app = $datos->app;
        $apm = $datos->apm;
        $carrera=$datos->carrera;
        $cuatrimestre=$datos->cuatrimestre;
        $email = $datos->email;
        $cel = $datos->cel;
        $curp = $datos->curp;
        $password = $datos->password;
        $id_rol=3;
        $grupo=$datos->grupo;
        $validar = $datos->validar;

        if($password != $validar){
            return view("registro",["estatus"=> "error", "mensaje"=> "¡Las contraseñas son diferentes!"]);
        }

        $usuario = new usuario();

        $usuario->nombres = $nombres;
        $usuario->apellido_paterno = $app;
        $usuario->apellido_materno = $apm;
        $usuario->carrera=$carrera;
        $usuario->cuatrimestre=$cuatrimestre;
        $usuario->correo = $email;
        $usuario->nomero_celular = $cel;
        $usuario->curp = $curp;
        $usuario->contrasenia = bcrypt($password);
        $usuario->role_id=$id_rol;
        $usuario->grupo=$grupo;
        $usuario->save();
        return view("inicioSesion",["estatus"=> "success", "mensaje"=> "¡Cuenta Creada!"]);
    }



    public function altaAlumno(request $datos){
        $usuario = usuario::where('correo',$datos->email)->first();
        if($usuario){
            return view("altaAlumno",["estatus"=> "error", "mensaje"=> "¡El correo ya se encuentra registrado!"]);
        }

        $nombres = $datos->nombre;
        $app = $datos->app;
        $apm = $datos->apm;
        $carrera=$datos->carrera;
        $cuatrimestre=$datos->cuatrimestre;
        $email = $datos->email;
        $cel = $datos->cel;
        $curp = $datos->curp;
        $rol=3;
        $grupo=$datos->grupo;
        $password = $datos->password;
        $validar = $datos->validar;

        if($password != $validar){
            return view("altaAlumno",["estatus"=> "error", "mensaje"=> "¡Las contraseñas son diferentes!"]);
        }

        $usuario = new usuario();

        $usuario->nombres = $nombres;
        $usuario->apellido_paterno = $app;
        $usuario->apellido_materno = $apm;
        $usuario->carrera=$carrera;
        $usuario->cuatrimestre=$cuatrimestre;
        $usuario->correo = $email;
        $usuario->nomero_celular = $cel;
        $usuario->curp = $curp;
        $usuario->contrasenia = bcrypt($password);
        $usuario->role_id=$rol;
        $usuario->grupo=$grupo;

        $usuario->save();
        return view("altaAlumno",["estatus"=> "success", "mensaje"=> "¡Alumno Registrado Exitosamente!"]);
    }




    public function altaAdminr(request $datos){
        $usuario = usuario::where('correo',$datos->email)->first();
        if($usuario){
            return view("altaAlumno",["estatus"=> "error", "mensaje"=> "¡El correo ya se encuentra registrado!"]);
        }

        $nombres = $datos->nombre;
        $app = $datos->app;
        $apm = $datos->apm;
        $email = $datos->email;
        $cel = $datos->cel;
        $rol=1;
        $password = $datos->password;
        $validar = $datos->validar;

        if($password != $validar){
            return view("altaAlumno",["estatus"=> "error", "mensaje"=> "¡Las contraseñas son diferentes!"]);
        }

        $usuario = new usuario();

        $usuario->nombres = $nombres;
        $usuario->apellido_paterno = $app;
        $usuario->apellido_materno = $apm;
        $usuario->correo = $email;
        $usuario->nomero_celular = $cel;
        $usuario->contrasenia = bcrypt($password);
        $usuario->role_id=$rol;
        $usuario->save();
        return view("altaAdmin",["estatus"=> "success", "mensaje"=> "¡Administrador Registrado Exitosamente!"]);
    }





    public function altaProfesorR(request $datos){
        $usuario = usuario::where('correo',$datos->email)->first();
        if($usuario){
            return view("altaAlumno",["estatus"=> "error", "mensaje"=> "¡El correo ya se encuentra registrado!"]);
        }

        $nombres = $datos->nombre;
        $app = $datos->app;
        $apm = $datos->apm;
        $email = $datos->email;
        $cel = $datos->cel;
        $rol=2;
        $password = $datos->password;
        $validar = $datos->validar;

        if($password != $validar){
            return view("altaProfesor",["estatus"=> "error", "mensaje"=> "¡Las contraseñas son diferentes!"]);
        }

        $usuario = new usuario();

        $usuario->nombres = $nombres;
        $usuario->apellido_paterno = $app;
        $usuario->apellido_materno = $apm;
        $usuario->correo = $email;
        $usuario->nomero_celular = $cel;
        $usuario->contrasenia = bcrypt($password);
        $usuario->role_id=$rol;
        $usuario->save();
        return view("altaProfesor",["estatus"=> "success", "mensaje"=> "¡Profesor Registrado Exitosamente!"]);
    }

    public function VerificarUsuario(Request $datos){

        $usuario=usuario::where('correo',$datos->email)->first();
        if (!$usuario){
            return view("inicioSesion",["estatus"=> "error", "mensaje"=> "¡Correo incorrecto!"]);
        }

        if (!Hash::check($datos->password,$usuario->contrasenia)){
            return view("inicioSesion",["estatus"=> "error", "mensaje"=> "¡Contraseña incorrecto!"]);
        }

        Session::put('usuario',$usuario);
        if(isset($datos->url)){
            $url = decrypt($datos->url);
            return redirect($url);
        }else{
            return redirect()->route('usuario.perfil');
        }
    }
    public function cerrarSesion(){
        if (Session::has('usuario')){
            Session::forget('usuario');
        }
        return redirect()->route('bienvenido');
    }
    public function editar(request $datos){
        $matricula=$datos->matricula;
        $correo=$datos->email;
        $cel=$datos->cel;
        $contrasenia=$datos->password;
        $validar=$datos->validar;
        $usuario=usuario::where('matricula',$matricula);
        $usuario->correo = $correo;
        $usuario->nomero_celular = $cel;
        $usuario->contrasenia = bcrypt($contrasenia);
        $usuario->save();
        return view("perfil",["estatus"=> "success", "mensaje"=> "Datos Actualizados Correctamente"]);
    }
    public function mostrarProfesores(){
        $profesores=usuario::all()->where('role_id',2);
        return view('mostrarProfesores',["profesores" => $profesores]);
    }
    public function mostrarAlumnos(){
        $alumnos=usuario::all()->where('role_id',3);
        return view('mostrarAlumnos',["alumnos" => $alumnos]);
    }

    public function eliminar(request $datos){
        $id=$datos->idBuscado;
        $usuario=usuario::find($id);
        if($usuario->role_id ==3){
            $usuario->delete();
            return view("PerfilAdmin",["estatus"=> "success", "mensaje"=> "Usuario Eliminado Correctamente"]);
        }
        else if ($usuario->role_id == 2){
            $usuario->delete();
            $materia=Materia::where('id_maestro',$id)->delete();
            return view("PerfilAdmin",["estatus"=> "success", "mensaje"=> "Profesor y materia eliminados correctamente"]);
        }else{
            return view("PerfilAdmin",["estatus"=> "error", "mensaje"=> "Usuario no Encontrado"]);
        }
    }




}
