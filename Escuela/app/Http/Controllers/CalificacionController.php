<?php

namespace App\Http\Controllers;

use App\Models\Calificaciones;
use Illuminate\Http\Request;
use MongoDB\Driver\Session;
use PDF;

class CalificacionController extends Controller
{
    public function subir(request $datos ){
        $id_alum=$datos->ida;
        $nombre=$datos->nombrea;
        $calif=new Calificaciones();
        $calif->id_alumno=$datos->ida;
        $calif->nombre_completo=$datos->nombrea;
        $calif->calificacion=$datos->cala;
        $calif->save();
        return view("perfilprofesor",["estatus"=> "success", "mensaje"=> "¡Calificaciones Acualizadas!"]);
    }
    public function misCalificaciones(){
        $calificaciones=Calificaciones::all()->where('id_alumno',Session('usuario')->id);
            return view('misCalificaciones',["calificaciones" => $calificaciones]);
    }
    public function imprimir(){
        $datos = Calificaciones::where('id_alumno',Session('usuario')->id)->get();
        $data = compact('datos');
        $imp = PDF::loadView('pdd.pdfmisCalificaciones', $data);
        return $imp->stream();
    }
    public function grafica(){
        $calificaciones=Calificaciones::all()->where('id_alumno',Session('usuario')->id);
        return view('calificacionesGraficadas',["calificaciones" => $calificaciones]);
    }
}
