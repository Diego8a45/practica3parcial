<?php

namespace App\Http\Controllers;

use App\Models\inscripcion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use MongoDB\Driver\Session;

class InscripcionController extends Controller
{
    public function inscripciones(){
        return view('inscripcion');
    }
    public function subir(request $datos){
        $usuario = inscripcion::where('id_alumno',Session('usuario')->id)->first();
        if($usuario){
            return view("inscripcion",["estatus"=> "error", "mensaje"=> "¡Ya has subido los Documentos!"]);
        }

        $id=Session('usuario')->id;
        $ine=$datos->file('ine')->store('public/images');
        $curp=$datos->file('curp')->store('public/images');
        $acta=$datos->file('acta')->store('public/images');
        $url1=Storage::url($ine);
        $url2=Storage::url($curp);
        $url3=Storage::url($acta);
        $ins=new inscripcion();
        $ins->id_alumno=$id;
        $ins->ine=$url1;
        $ins->curp=$url2;
        $ins->ActaNacimiento=$url3;
        $ins->save();
        return view("inscripcion",["estatus"=> "success", "mensaje"=> "¡Documentos Guardados Exitosamente!"]);
    }
}
