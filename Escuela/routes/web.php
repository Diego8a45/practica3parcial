<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AlumnoController;
use App\Http\Controllers\MateriasController;
use App\Http\Controllers\CalificacionController;
use App\Http\Controllers\InscripcionController;


Route::get('/',[AlumnoController::class,'bienvenido'])->name('bienvenido');

Route::get('/nosotros',[AlumnoController::class,'nosotros'])->name('nosotros');
Route::get('/cerrarSesion',[AlumnoController::class,'cerrarSesion'])->name('cerrar.sesion');
Route::get('/carrerasUniversitarias',[AlumnoController::class,'carreras'])->name('carreras');
Route::get('/registrarUsuario',[AlumnoController::class,'registro'])->name('registro');
Route::get('/inicioDeSesion',[AlumnoController::class,'inicioSesion'])->name('inicioSesion');
Route::post('/registrarUsuario',[AlumnoController::class,'registrar'])->name('registro.form');
Route::post('/inicioDeSesion',[AlumnoController::class,'VerificarUsuario'])->name('inicioSesion.form');

Route::prefix('/usuario')->middleware("VerificarAlumno")->group(function (){
    Route::get('/perfil',[AlumnoController::class,'perfil'])->name('usuario.perfil');
    Route::get('/editarDatos',[AlumnoController::class,'editarDatos'])->name('usuario.editarDatos');
    Route::post('/editarDatos',[AlumnoController::class,'editar'])->name('usuario.editarDatos.form');
    Route::prefix('/alta')->group(function (){
        Route::get('/Alumno',[AlumnoController::class,'alta'])->name('usuario.altaAlumno');
        Route::post('/Alumno',[AlumnoController::class,'altaAlumno'])->name('usuario.altaAlumno.form');
        Route::get('/Profesor',[AlumnoController::class,'altaProfesor'])->name('usuario.altaProfesor');
        Route::post('/Profesor',[AlumnoController::class,'altaProfesorR'])->name('usuario.altaProfesor.form');
        Route::get('/Administrador',[AlumnoController::class,'altaAdmin'])->name('usuario.altaAdmin');
        Route::post('/Administrador',[AlumnoController::class,'altaAdminr'])->name('usuario.altaAdmin.form');
        Route::get('/materia',[MateriasController::class,'crearMateria'])->name('usuario.altaMateria');
        Route::post('/materia',[MateriasController::class,'altaMateria'])->name('usuario.altaMateria.form');
    });
    Route::get('/mostrarMateria',[MateriasController::class,'mostrarMaterias'])->name('usuario.mostrarMateria');
    Route::get('/mostrarProfesores',[AlumnoController::class,'mostrarProfesores'])->name('usuario.mostrarProfesores');
    Route::get('/mostrarAlumnos',[AlumnoController::class,'mostrarAlumnos'])->name('usuario.mostrarAlumnos');
    Route::post('/mostrarAlumnos',[AlumnoController::class,'eliminar'])->name('usuario.eliminarAlumno');
    Route::post('/mostrarMateria',[MateriasController::class,'eliminarMateria'])->name('usuario.eliminarMateria');
    Route::get('/materiasAsignadas',[MateriasController::class,'buscarMaterias'])->name('usuario.materiasAsignadas');
    Route::post('/materiasAsignadas',[MateriasController::class,'mostrarAlumnoMateria'])->name('usuario.materiasAsignadas.form');
    Route::post('/Calificsciones',[CalificacionController::class,'subir'])->name('usuario.calificaciones.form');
    Route::get('/misCalificaciones',[CalificacionController::class,'misCalificaciones'])->name('usuario.misCalificaciones');
    Route::get('/Graficas',[CalificacionController::class,'grafica'])->name('usuario.calificacionesGraficadas');
    Route::get('/pdf',[CalificacionController::class,'imprimir'])->name('usuario.imprimir');
    Route::get('/inscripcion',[InscripcionController::class,'inscripciones'])->name('usuario.inscripcion');
    Route::post('/inscripcion',[InscripcionController::class,'subir'])->name('usuario.inscripcion.form');
});







