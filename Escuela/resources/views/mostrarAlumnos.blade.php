@extends('layout.contenidoAdmin')

@section('titulo')
    <title>Alumnos</title>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap5.min.js">

@endsection

@section('contenido')
            <table id="profesores" class="table">
                <thead>
                <tr>
                    <th>Matricula</th>
                    <th>Nombre</th>
                    <th>Carrera</th>
                    <th>Cuatrimestre</th>
                </tr>
                </thead>
                <tbody>
                @foreach($alumnos as $alumno)
                    <tr>
                        <td>{{$alumno->id}}</td>
                        <td>{{$alumno->nombres}} {{$alumno->apellido_paterno}} {{$alumno->apellido_materno}}</td>
                        <td>{{$alumno->carrera}}</td>
                        <td>{{$alumno->cuatrimestre}}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <form method="post" action="{{route('usuario.eliminarAlumno')}}">
                {{csrf_field()}}
                <div class="mb-3 mt-2 d-flex ">
                    <label for="address2" class="mx-2">Eliminar Alumno</label>
                    <input type="text" class="form-control w-25 mx-2" name="idBuscado" placeholder="Ingrese matricula" required>
                    <input type="submit" class="link w-25 btn btn-primary btn-lg mx-2" type="submit" value="Eliminar">
                </div>
            </form>
@endsection
@section('js')
@endsection
