@extends('layout.contenidoProfesor')

@section('titulo')
    <title>Mis Materias Asignadas</title>
@endsection

@section('css')

@endsection

@section('contenido')
    <table id="profesores" class="table bg-white">
        <thead>
        <tr>
            <th>Matricula</th>
            <th>Nombre</th>
            <th>Grupo</th>
            <th>Calificación</th>

        </tr>
        </thead>
        <tbody>
        @foreach($alum as $alumn)
            <tr>
                <td>{{$alumn->id}}</td>
                <td>{{$alumn->nombres}} {{$alumn->apellido_paterno}} {{$alumn->apellido_materno}}</td>
                <td>{{$alumn->grupo   }}</td>
                <td>
                    <form method="post" action="{{route('usuario.calificaciones.form')}}">
                        {{csrf_field()}}
                        <input type="hidden" value="{{$alumn->id}}" name="ida">
                        <input type="hidden" value="{{$alumn->nombres}} {{$alumn->apellido_paterno}} {{$alumn->apellido_materno}}" name="nombrea">
                        <input type="text" name="cala">
                        <input type="submit" class="btn-warning" value="subir">
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('js')
@endsection
