@extends('layout.contenidoEstudiante')

@section('titulo')
    <title>Inscripciones</title>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap5.min.js">
@endsection

@section('contenido')
    @if(isset($estatus))
        @if($estatus == "success")
            <label class="text-success">{{$mensaje}}</label>
        @elseif($estatus == "error")
            <label class="text-warning">{{$mensaje}}</label>
        @endif
    @endif
    <div class="row col-10 bg-dark text-white mt-5 mx-auto">
        <h1 class="text-center mt-3 mb-2"> Subir Documentos</h1>
        <form method="post" enctype="multipart/form-data" action="{{route('usuario.inscripcion.form')}}">
            {{csrf_field()}}
            <div class="mb-3">
                <label for="email">Adjuntar INE</label>
                <input type="file" class="form-control" name="ine"  required>
            </div>
            <div class="mb-3">
                <label for="email">Adjuntar CURP</label>
                <input type="file" class="form-control" name="curp" required>
            </div>
            <div class="mb-3">
                <label for="email">Adjuntar Acta de nacimiento</label>
                <input type="file" class="form-control" name="acta" required>
            </div>
            <input type="submit" class="link w-100 btn btn-primary btn-lg mb-5" type="submit" value="Subir Documentos">
        </form>
    </div>
@endsection
@section('js')
@endsection

