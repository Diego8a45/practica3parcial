@extends('layout.contenidoAdmin')

@section('titulo')
    <title>Registrar Alumno</title>
@endsection

@section('css')

@endsection

@section('contenido')
    <div class="container text-white">
        <form  method="post" action="{{route('usuario.altaAlumno.form')}}">
            {{csrf_field()}}
            <center><h1 class="display-6 fw-bold">Registro de Estudiantes</h1></center>
            <label class="text-danger">
                @if(isset($estatus))
                    <label class="text-danger">{{$mensaje}}</label>
                @endif
            </label>
            <div class="row">
                <div class="col-md-4 mb-3">
                    <label for="nombre">Nombre(s):</label>
                    <input type="text" class="form-control" name="nombre" placeholder="" value="" required>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="lastName">Apellido Paterno</label>
                    <input type="text" class="form-control" name="app" placeholder="" value="" required>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="lastName">Apellido Materno</label>
                    <input type="text" class="form-control" name="apm" placeholder="" value="" required>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <label for="carrera" class="form-label">Carrera</label>
                    <select class="form-select" name="carrera">
                        <option value="">Elige...</option>
                        <option value="Ingenieria de Software">Ingenieria de Software</option>
                        <option value="Ingenieria Mecanica Automotriz">Ingenieria Mecanica Automotriz</option>
                        <option value="Ingenieria en Manufactura">Ingenieria en Manufactura</option>
                        <option value="Licenciatura en Negocios Internacionales">Licenciatura en Negocios Internacionales</option>
                        <option value="Licenciatura en Derecho">Licenciatura en Derecho</option>
                        <option value="Licenciatura en Mercadotecnia">Licenciatura en Mercadotecnia</option>
                    </select>
                </div>

                <div class="col-md-4">
                    <label for="carrera" class="form-label">Cuatrimestre</label>
                    <select class="form-select" name="cuatrimestre">
                        <option value="">Elige...</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="grupo" class="form-label">Grupo</label>
                    <select class="form-select" name="grupo">
                        <option value="">Elige...</option>
                        <option value="2121">2121</option>
                        <option value="2221">2221</option>
                        <option value="2321">2321</option>
                        <option value="2421">2421</option>
                        <option value="2521">2521</option>
                        <option value="2621">2621</option>
                        <option value="2721">2721</option>
                        <option value="2821">2821</option>
                        <option value="2921">2921</option>
                        <option value="3021">3021</option>
                    </select>
                </div>
            </div>

            <div class="mb-3">
                <label for="email">Correo</label>
                <input type="email" class="form-control" name="email" placeholder="tunombre@ejemplo.com" required>
            </div>
            <div class="mb-3">
                <label for="address2">Numero de celular</label>
                <input type="text" class="form-control" name="cel" placeholder="" required>
            </div>
            <div class="mb-3">
                <label for="address2">Curp</label>
                <input type="text" class="form-control" name="curp" placeholder="" required>
            </div>
            <div class="mb-3">
                <label for="address2">Contraseña</label>
                <input type="password" class="form-control" name=password placeholder="" required>
            </div>
            <div class="mb-3">
                <label for="address2">Verificar contraseña</label>
                <input type="password" class="form-control" name="validar" placeholder="" required>
            </div>
            <input type="submit" class="link w-100 btn btn-primary btn-lg" type="submit" value="Crear Cuenta">

            <div class="row">

            </div>
        </form>
    </div>
@endsection

@section('js')
@endsection
