@extends('layout.contenidoEstudiante')

@section('titulo')
    <title>Mis Calificaciones</title>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap5.min.js">

@endsection

@section('contenido')
    <table id="profesores" class="table bg-light text-center">
        <thead class="bg-dark text-white ">
        <hi>Estudiante: {{Session('usuario')->nombres}} {{Session('usuario')->apellido_paterno}} {{Session('usuario')->apellido_materno}} </hi>
        <tr>
            <th>Materia</th>
            <th>Calificación</th>
        </tr>
        </thead>
        <tbody>
        @foreach($datos as $dato)
            <tr>
                <td>{{$dato->materia}}</td>
                <td>{{$dato->calificacion}}</td>
            </tr>
        @endforeach

        </tbody>
    </table>
    <div class="col-3 mx-auto">
        <a class="btn btn-success" href="{{route('usuario.imprimir')}}">Imprimir PDF</a>
    </div>
@endsection
@section('js')
@endsection

