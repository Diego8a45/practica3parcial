@extends('layout.contenidoProfesor')

@section('titulo')
    <title>Mis Materias Asignadas</title>
@endsection

@section('css')

@endsection

@section('contenido')
        <div class="row mx-auto mb-5">
            <form method="post" action="{{route('usuario.materiasAsignadas.form')}}">
                {{csrf_field()}}
                <div class="col-4 mx-auto">
                    <label for="carrera" class="form-label">Materia</label>
                    <select class="form-select" name="materia">
                        <option value="">Elige...</option>
                        @foreach($mismaterias as $mat)
                            <option value="{{$mat->nombre_materia}}">{{$mat->nombre_materia}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-4 mx-auto">
                    <label for="carrera" class="form-label">Grupo</label>
                    <select class="form-select" name="grupo">
                        <option value="">Elige...</option>
                        @foreach($mismaterias as $mat)
                        <option value="{{$mat->grupo_asignado}}">{{$mat->grupo_asignado}}</option>
                        @endforeach()
                    </select>
                </div>
                <div class="col-2 mx-auto mt-5">
                    <input type="submit" class="btn-success col-12" value="mostrar">
                </div>
            </form>
        </div>
@endsection

@section('js')
@endsection
