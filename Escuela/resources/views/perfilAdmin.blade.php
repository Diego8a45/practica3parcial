@extends('layout.contenidoAdmin')

@section('titulo')
    <title>{{session('usuario')->nombres}}</title>
@endsection

@section('css')
    <link href="/vendor/perfil.css" rel="stylesheet">
@endsection

@section('contenido')
    <div class="list-group">
        <label class="text-success">
            @if(isset($estatus))
                @if($estatus == "success")
                    <label class="text-white">{{$mensaje}}</label>
                @elseif($estatus == "error")
                    <label class="text-warning">{{$mensaje}}</label>
                @endif
            @endif
        </label>
        <div class="">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">Nombre completo del alumno:</h5>
            </div>
            <p class="mb-1 display-6 text-dark">{{session('usuario')->nombres}} {{session('usuario')->apellido_paterno}} {{session('usuario')->apellido_materno}}</p>
        </div>
        <div class="border-top my-1">
            <div class="te d-flex w-100 justify-content-between">
                <h5 class="mb-1">Matricula:</h5>
            </div>
            <p class="mb-1 display-6 text-dark">{{session('usuario')->id}}</p>
        </div>
        <div class="border-top my-1">
            <div class="te d-flex w-100 justify-content-between">
                <h5 class="mb-1">Correo:</h5>
            </div>
            <p class="mb-1 display-6 text-dark">{{session('usuario')->correo}}</p>
        </div>
        <div class="border-top my-1">
            <div class="te d-flex w-100 justify-content-between">
                <h5 class="mb-1">Numero de celular:</h5>
            </div>
            <p class="mb-1 display-6 text-dark">{{session('usuario')->nomero_celular}}</p>
        </div>
    </div>
@endsection

@section('js')

@endsection
