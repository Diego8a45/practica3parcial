@extends('layout.contenido')

@section('titulo')
    <title>Carreras Universitarias</title>
@endsection

@section('css')
    <link href="/vendor/carreras.css" rel="stylesheet">
@endsection

@section('contenido')
    <div class="container">
        <center><h1 class="display-6 fw-bold">INGENIERIAS</h1></center>
        <div class="row">
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">

                        <img src="{{asset('images/mecanico.jpg')}}" class="imagen d-block mx-auto img-fluid" alt="Estudia en USLLM" loading="lazy">

                    <div class="card-body">
                        <center><h1 class="display-6 fw-bold">Mecanica Automotriz</h1></center>
                        <p class="card-text text-center">La ingeniería Mecánica Automotriz se enfoca en tres áreas: Mantenimiento, Desarrollo Tecnológico y Manufactura, donde se elaboran programas de mantenimiento, supervisa recursos humanos, tecnológicos y materiales  .</p>
                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">

                    <img src="{{asset('images/software.jpg')}}" class="imagen d-block mx-auto img-fluid" alt="Estudia en USLLM" loading="lazy">

                    <div class="card-body">
                        <center><h1 class="display-6 fw-bold">Software</h1></center>
                        <p class="card-text text-center">La Ingeniería de Software es una de las ramas de las ciencias de la computación que estudia la creación de software confiable y de calidad, basándose en métodos y técnicas de ingeniería. Brindando soporte operacional y de mantenimiento, el campo de estudio de la ingeniería.</p>
                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">

                    <img src="{{asset('images/manufactura.jpg')}}" class="imagen d-block mx-auto img-fluid" alt="Estudia en USLLM" loading="lazy">

                    <div class="card-body">
                        <center><h1 class="display-6 fw-bold">Manufactura</h1></center>
                        <p class="card-text text-center">La ingeniería de manufactura es una rama de la ingeniería que incluye la investigación, diseño y desarrollo de sistemas, procesos, máquinas, herramientas y equipos. El enfoque principal del ingeniero de manufactura es convertir la materia prima en un producto.</p>
                    </div>
                </div>
            </div>
        </div>



        <center><h1 class="display-6 fw-bold ">LICENCIATURAS</h1></center>
        <div class="row">
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">

                    <img src="{{asset('images/mercadotecnia.jpg')}}" class="imagen d-block mx-auto img-fluid" alt="Estudia en USLLM" loading="lazy">

                    <div class="card-body">
                        <center><h1 class="display-6 fw-bold ">Mercadotecnia</h1></center>
                        <p class="card-text text-center">La Licenciatura en Mercadotecnia atiende las necesidades de crecimiento y desarrollo de las empresas nacionales, aportando soluciones y estrategias para aumentar ventas y posicionar marcas. Además, ayuda al mercado mexicano a través de la consolidación de empresas a nivel nacional e internacional.</p>
                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">

                    <img src="{{asset('images/negocios.jpg')}}" class="imagen d-block mx-auto img-fluid" alt="Estudia en USLLM" loading="lazy">

                    <div class="card-body">
                        <center><h1 class="display-6 fw-bold">Negocios Internacionales</h1></center>
                        <p class="card-text text-center">La licenciatura en Negocios Internacionales ofrece una formación sólida en administración y negocios, economía y asuntos globales, que se complementa con conocimientos generales de las TIC y métodos cuantitativos, de ética y sociología, así como del dominio de la lengua extranjera.</p>
                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">

                    <img src="{{asset('images/derecho.jpg')}}" class="imagen d-block mx-auto img-fluid" alt="Estudia en USLLM" loading="lazy">

                    <div class="card-body">
                        <center><h1 class="display-6 fw-bold">Derecho</h1></center>
                        <p class="card-text text-center">Licenciatura en Derecho, licenciatura en leyes, licenciatura en ciencias jurídicas o Grado en Derecho es el grado académico que se confiere a quienes han concluido exitosamente un proceso de estudios de Derecho en una universidad o institución de estudios superiores.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection

@section('js')

@endsection
