@extends('layout.contenidoAdmin')

@section('titulo')
    <title>Materias</title>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap5.min.js">

@endsection

@section('contenido')
            <table id="materias" class="table">
                <thead>
                <tr>
                    <th>Id materia</th>
                    <th>Nombre materia</th>
                    <th>Profesor asignado</th>
                    <th>Carrera</th>
                </tr>
                </thead>
                <tbody>
                @foreach($materiasR as $materia)
                    <tr>
                        <td>{{$materia->id}}</td>
                        <td>{{$materia->nombre_materia}}</td>
                        <td>{{$materia->nombre_maestro}}</td>
                        <td>{{$materia->carrera_asignada}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <form method="post" action="{{route('usuario.eliminarMateria')}}">
                {{csrf_field()}}
                <div class="mb-3 mt-2 d-flex ">
                    <label for="address2" class="mx-2">Eliminar Materia</label>
                    <input type="text" class="form-control w-25 mx-2" name="idMateria" placeholder="Ingrese matricula" required>
                    <input type="submit" class="link w-25 btn btn-primary btn-lg mx-2" type="submit" value="Eliminar">
                </div>
            </form>
@endsection
@section('js')
@endsection
