@extends('layout.contenido')

@section('titulo')
    <title>Universidad Las Lomas</title>
@endsection

@section('css')
    <link href="/vendor/bienvenido.css" rel="stylesheet">
@endsection

@section('contenido')
    <main class="container">
        <div class="p-4 p-md-5 mb-4">
            <div class="col-md-6 px-0">
                <h1 class="display-4 fst-italic">Bienvenido a nuestro blog</h1>
                <p class="lead my-3">te interesa formar parte de nuestros sistemas educativos, ¿quieres tener informacion acerca de nuestras ingenierias y licenciaturas?</p>
                <p class="lead mb-0"><a href="{{route('nosotros')}}" class="text-white fw-bold">Continuar leyendo...</a></p>
            </div>
        </div>
@endsection

@section('js')

@endsection
