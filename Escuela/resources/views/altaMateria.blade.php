@extends('layout.contenidoAdmin')

@section('titulo')
    <title>Registrar Materia</title>
@endsection

@section('css')

@endsection

@section('contenido')
    <div class="container text-white">
        <form  method="post" action="{{route('usuario.altaMateria.form')}}">
            {{csrf_field()}}
            <center><h1 class="display-6 fw-bold">Registro de Materia</h1></center>
            <label class="text-danger">
                @if(isset($estatus))
                    <label class="text-white">{{$mensaje}}</label>
                @endif
            </label>
            <div class="mb-3">
                <label for="email">Materia:</label>
                <input type="text" class="form-control" name="nombreMateria" placeholder="Matematicas" required>
            </div>
            <div class="mb-3">
                <label for="address2">Nombre del profesor:</label>
                <input type="text" class="form-control" name="profesor" placeholder="" required>
            </div>
            <div class="mb-3">
                <label for="address2">Id Profesor</label>
                <input type="text" class="form-control" name="idProfesor" placeholder="" required>
            </div>

            <div class="col-md-6">
                <label for="carrera" class="form-label">Carrera</label>
                <select class="form-select" name="carrera">
                    <option value="">Elige...</option>
                    <option value="Ingenieria de Software">Ingenieria de Software</option>
                    <option value="Ingenieria Mecanica Automotriz">Ingenieria Mecanica Automotriz</option>
                    <option value="Ingenieria en Manufactura">Ingenieria en Manufactura</option>
                    <option value="Licenciatura en Negocios Internacionales">Licenciatura en Negocios Internacionales</option>
                    <option value="Licenciatura en Derecho">Licenciatura en Derecho</option>
                    <option value="Licenciatura en Mercadotecnia">Licenciatura en Mercadotecnia</option>
                </select>
            </div>
            <div class="col-md-6">
                <label for="grupo" class="form-label">Grupo</label>
                <select class="form-select" name="grupo">
                    <option value="">Elige...</option>
                    <option value="2121">2121</option>
                    <option value="2221">2221</option>
                    <option value="2321">2321</option>
                    <option value="2421">2421</option>
                    <option value="2521">2521</option>
                    <option value="2621">2621</option>
                    <option value="2721">2721</option>
                    <option value="2821">2821</option>
                    <option value="2921">2921</option>
                    <option value="3021">3021</option>
                </select>
            </div>
            <input type="submit" class="link w-100 btn mt-5 btn-primary btn-lg" type="submit" value="Crear Materia">
            <div class="row">
            </div>

        </form>
    </div>
@endsection

@section('js')
@endsection
