@extends('layout.contenodpérfil')

@section('titulo')
    <title>Registrar Alumno</title>
@endsection

@section('css')
@endsection

@section('contenido')
    <div class="container text-secondary">
        <form  method="post" action="{{route('usuario.editarDatos.form')}}">
            {{csrf_field()}}
            <center><h1 class="display-6 fw-bold text-secondary">Editar Datos</h1></center>
            <label class="text-danger">
            </label>
            <div class="row">
                <div class="col-md-4 mb-3">
                    <label for="nombre">Nombre(s):</label>
                    <input type="text" class="form-control" name="nombre" placeholder="" value="{{session('usuario')->nombres}}" disabled>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="lastName">Apellido Paterno</label>
                    <input type="text" class="form-control" name="app" placeholder="" value="{{session('usuario')->apellido_paterno}}" disabled>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="lastName">Apellido Materno</label>
                    <input type="text" class="form-control" name="apm" placeholder="" value="{{session('usuario')->apellido_materno}}" disabled>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="lastName">Matricula</label>
                    <input type="text" class="form-control" name="matricula" placeholder="" value="{{session('usuario')->matricula}}" disabled>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="carrera" class="form-label">Carrera</label>
                    <select class="form-select" name="carrera"  disabled>
                        <option value="">{{session('usuario')->carrera}}</option>
                    </select>
                </div>

                <div class="col-md-6">
                    <label for="carrera" class="form-label">Cuatrimestre</label>
                    <select class="form-select" name="cuatrimestre" disabled>
                        <option value="">{{session('usuario')->cuatrimestre}}</option>
                    </select>
                </div>
            </div>

            <div class="mb-3">
                <label for="email">Correo</label>
                <input type="email" class="form-control" name="email" value="{{session('usuario')->correo}}">
            </div>
            <div class="mb-3">
                <label for="address2">Numero de celular</label>
                <input type="text" class="form-control" name="cel" value="{{session('usuario')->nomero_celular}}">
            </div>
            <div class="mb-3">
                <label for="address2">Contraseña</label>
                <input type="password" class="form-control" name=password placeholder="" required>
            </div>
            <div class="mb-3">
                <label for="address2">Verificar contraseña</label>
                <input type="password" class="form-control" name="validar" placeholder="" required>
            </div>
            <input type="submit" class="link w-100 btn btn-primary btn-lg" type="submit" value="Actualizar datos">

            <div class="row">

            </div>
        </form>
    </div>
@endsection

@section('js')
@endsection
