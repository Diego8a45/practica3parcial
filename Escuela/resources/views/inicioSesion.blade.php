@extends('layout.contenido')

@section('titulo')
    <title>Iniciar Sesión</title>
@endsection

@section('css')
    <link href="/vendor/inicioSesion.css" rel="stylesheet">
@endsection

@section('contenido')

    </main>
    <div class="container">
        @if(isset($estatus))
            @if($estatus == "success")
                <label class="text-success">{{$mensaje}}</label>
            @elseif($estatus == "error")
                <label class="text-warning">{{$mensaje}}</label>
            @endif
        @endif
        <form method="post" action="{{route('inicioSesion.form')}}">
            {{csrf_field()}}
            <center> <img class="mb-4" src="{{asset('images/universidad.jpg')}}" alt="" width="100" height="100"></center>
            <h1 class="h3 mb-3 fw-normal text-center">Inicio de Sesión</h1>

            <div class="form-floating">
                <input name="email" class="form-control" id="floatingInput" placeholder="name@example.com">
                <label for="floatingInput">Email address</label>
            </div>
            <div class="form-floating">
                <input type="password" class="form-control" name="password" placeholder="Password">
                <label for="floatingPassword">Password</label>
            </div>

            <div class="checkbox mb-3">
                <label>
                    <input type="checkbox" value="remember-me"> Remember me
                </label>
            </div>
            <button class="w-100 btn btn-lg btn-primary" type="submit">Iniciar <Sesión></Sesión></button>
        </form>
    </div>
@endsection

@section('js')
@endsection

