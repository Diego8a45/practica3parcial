@extends('layout.contenido')

@section('titulo')
    <title>Universidad Las Lomas</title>
@endsection

@section('css')
    <link href="/vendor/nosotros.css" rel="stylesheet">
@endsection

@section('contenido')
    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner h-100">
            <div class="carousel-item active">
                <img src="{{asset('images/estudiante.jpg')}}" class="d-block w-100 align-content-center" alt="...">
            </div>
            <div class="carousel-item">
                <img src="{{asset('images/negocios.jpg')}}" class="d-block w-100 align-content-center" alt="...">
            </div>
            <div class="carousel-item">
                <img src="{{asset('images/estudiar.jpg')}}" class="d-block w-100 align-content-center" alt="...">
            </div>
        </div>
        <a class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
             <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </a>
        <a class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </a>
    </div>


    <div class="container">
        <div class="cuadro px-4 pt-5 my-5 text-center border-bottom border-top">
            <h1 class="display-4 fw-bold">Conoce nuestras ingenierias y licenciaturas</h1>
            <div class="col-lg-6 mx-auto">
                <p class="lead mb-4">Obten una beca del 45% de descuento en cualquiera de nuestras ingenierias o lincenciaturas. ¿No conoces nuestas carreras? que esperas para echarles un vistazo.</p>
                <div class="d-grid gap-2 d-sm-flex justify-content-sm-center mb-5">
                    <a href="{{route('carreras')}}" class="btn btn-primary btn-lg px-4 me-sm-3 mx-1 mt-2">Ingenierias</a>
                    <a href="{{route('carreras')}}" class="btn btn-primary btn-lg px-4 me-sm-3 mx-1 mt-2">Lincenciaturas</a>
                </div>
            </div>
        </div>

        <div class="cuadro1 px-3 my-5 text-center border-bottom border-top">
            <div class="row flex-lg-row-reverse align-items-center  py-5">
                <div class="col-12 col-sm-12 col-lg-6">
                    <img src="{{asset('images/estudiante.jpg')}}" class="imagen d-block mx-auto img-fluid" alt="Estudia en USLLM" loading="lazy">
                </div>
                <div class="col-lg-6">
                    <h1 class="display-5 fw-bold lh-1 mb-3">¿Por que estudiar con nosotros?</h1>
                    <p class="lead">¿Sabías? que actualmente contamos con mas de 100 becarios en el extranjero, asi mismo con estudiantes que han reallizado estadias en las empresas más reconocidas a nivel mundial.</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

@endsection
