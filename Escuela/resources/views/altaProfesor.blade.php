@extends('layout.contenidoAdmin')

@section('titulo')
    <title>Registrar Profesor</title>
@endsection

@section('css')

@endsection

@section('contenido')
    <div class="container text-white">
        <form  method="post" action="{{route('usuario.altaProfesor.form')}}">
            {{csrf_field()}}
            <center><h1 class="display-6 fw-bold">Registro de Profesor</h1></center>
            <label class="text-danger">
                @if(isset($estatus))
                    <label class="text-danger">{{$mensaje}}</label>
                @endif
            </label>
            <div class="row">
                <div class="col-md-4 mb-3">
                    <label for="nombre">Nombre(s):</label>
                    <input type="text" class="form-control" name="nombre" placeholder="" value="" required>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="lastName">Apellido Paterno</label>
                    <input type="text" class="form-control" name="app" placeholder="" value="" required>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="lastName">Apellido Materno</label>
                    <input type="text" class="form-control" name="apm" placeholder="" value="" required>
                </div>
            </div>
            <div class="mb-3">
                <label for="email">Correo</label>
                <input type="email" class="form-control" name="email" placeholder="tunombre@ejemplo.com" required>
            </div>
            <div class="mb-3">
                <label for="address2">Numero de celular</label>
                <input type="text" class="form-control" name="cel" placeholder="" required>
            </div>
            <div class="mb-3">
                <label for="address2">Contraseña</label>
                <input type="password" class="form-control" name=password placeholder="" required>
            </div>
            <div class="mb-3">
                <label for="address2">Verificar contraseña</label>
                <input type="password" class="form-control" name="validar" placeholder="" required>
            </div>
            <input type="submit" class="link w-100 btn btn-primary btn-lg" type="submit" value="Crear Cuenta">

            <div class="row">

            </div>
        </form>
    </div>
@endsection

@section('js')
@endsection
