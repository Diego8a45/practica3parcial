<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    @yield('titulo')
    <link rel="stylesheet" href="{{asset('css/layout2.css')}}">
    @yield('css')
</head>
<body>
<header class="p-3 mb-3 border-bottom">
    <div class="container">
        <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            <a href="/" class="titulo d-flex align-items-center mb-2 mb-lg-0 mx-5 text-dark text-decoration-none">
                UNLLM
            </a>

            <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                <li><a href="{{route('usuario.calificacionesGraficadas')}}" class="nav-link px-2 link-dark">Grafica de calificaciones</a></li>
                <li><a href="{{route('usuario.misCalificaciones')}}" class="nav-link px-2 link-dark">Mis Calificaciones</a></li>
                <li>
                </li>
            </ul>

            <div class="dropdown text-end mx-4">
                <a href="#" class="d-block link-dark text-decoration-none dropdown-toggle align-items-center" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                    Tramites
                </a>
                <ul class="dropdown-menu text-small" aria-labelledby="dropdownUser1">
                    <li><a class="dropdown-item" href="{{route('usuario.inscripcion')}}">Reinscripción</a></li>
                </ul>
            </div>

            <div class="dropdown text-end">
                <a href="#" class="d-block link-dark text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                    <img src="https://github.com/mdo.png" alt="mdo" width="32" height="32" class="rounded-circle">
                </a>
                <ul class="dropdown-menu text-small" aria-labelledby="dropdownUser1">
                    <li><a class="dropdown-item" href="{{route('usuario.editarDatos')}}">Configuración</a></li>
                    <li><a class="dropdown-item" href="{{route('usuario.perfil')}}">Perfil</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="{{route('cerrar.sesion')}}">cerrar Sesión</a></li>
                </ul>
            </div>
            <a href="#" class="nav-link px-2 link-dark">Hola {{session('usuario')->nombres}}</a>
        </div>
    </div>
</header>
@yield('contenido')
@yield('js')

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
<style>
</style>
</body>
</html>
