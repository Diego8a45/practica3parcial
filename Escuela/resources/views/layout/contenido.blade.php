<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    @yield('titulo')
    <link rel="stylesheet" href="{{asset('css/layout.css')}}">
    @yield('css')
</head>
<body>
<header class="barra d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3 mb-4 border-bottom">
    <a href="{{route('bienvenido')}}" class="titulo d-flex align-items-center col-md-3 mb-2 mb-md-0 text-decoration-none">UNLLM</a>

    <ul class="nav col-12 col-md-auto mb-2 justify-content-center mb-md-0">
        <li><a href="{{route('nosotros')}}" class="nav-link px-2">Acerca de Nosotros</a></li>
        <li><a href="{{route('carreras')}}" class="nav-link px-2">Carreras Universitarias</a></li>
        <li><a href="{{route('bienvenido')}}" class="nav-link px-2">Misión y Visión</a></li>
    </ul>
    <div class="col-md-3 text-end">
        <a href="{{route('registro')}}" class="btn btn-outline-primary me-2 m">Registrarme</a>
        <a href="{{route('inicioSesion')}}" class="btn btn-outline-primary me-2">Iniciar Sesión</a>
    </div>
</header>
@yield('contenido')
@yield('js')
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
<style>
</style>
</body>
</html>
